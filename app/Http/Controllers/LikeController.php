<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function upsert(Request $request, Post $post)
    {
        $user = $request->user();

        $like = $post->likes()->where('user_id', $user->id)->first();

        if ($like){
            $like->update(['active' => !$like->active]);
        }else{
            $post->likes()->create([
                'user_id' => $user->id
            ]);
        }

        return back();
    }
}
