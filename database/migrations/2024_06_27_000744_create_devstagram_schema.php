<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevstagramSchema extends Migration
{
    protected const
        USERS = "users",
        IMAGES ='images',
        POSTS = "posts",
        COMMENTS = "comments",
        LIKES = "likes",
        FOLLOWERS = "followers";

    public function up()
    {
        self::createUsersTable(self::USERS);
        self::createImagesTable(self::IMAGES);
        self::createPostsTable(self::POSTS);
        self::createCommentsTable(self::COMMENTS);
        self::createLikesTable(self::LIKES);
        self::createFollowersTable(self::FOLLOWERS);
    }

    public function down()
    {
        $tables =
            [
                self::FOLLOWERS,
                self::LIKES,
                self::COMMENTS,
                self::POSTS,
                self::IMAGES,
                self::USERS,
            ];
        self::dropIfExistCustom($tables);
    }

    private static function dropIfExistCustom($tables)
    {
        foreach ($tables as $table){
            Schema::dropIfExists($table);
        }
    }

    /**
     * @property Table users
     */
    protected static function createUsersTable($tableName){
        Schema::create($tableName, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('active')->default(true);
            $table->string('remember_token')->nullable();
            $table->string('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * @property Table images
     */
    protected static function createImagesTable($tableName){
        Schema::create($tableName, function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->morphs('imageable');
            $table->timestamps();
        });
    }

    /**
     * @property Table posts
     */
    protected static function createPostsTable($tableName){
        Schema::create($tableName, function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * @property Table comments
     */
    protected static function createCommentsTable($tableName){
        Schema::create($tableName, function (Blueprint $table) {
            $table->id();
            $table->string('comment');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->foreignId('post_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * @property Table likes
     */
    protected static function createLikesTable($tableName){
        Schema::create($tableName, function (Blueprint $table) {
            $table->id();
            $table->boolean('active')->default(true);
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->foreignId('post_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * @property Table followers
     */
    protected static function createFollowersTable($tableName){
        Schema::create($tableName, function (Blueprint $table) {
            $table->id();
            $table->boolean('active')->default(true);
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->foreignId('follower_id')->constrained('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

}
