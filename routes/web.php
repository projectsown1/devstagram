<?php

use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\FollowerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('principal');
});

/*******************AUTH********************/
Route::get('/login',[AuthController::class,'index'])->name('login_index');
Route::post('/login',[AuthController::class,'login'])->name('login');
Route::post('/logout',[AuthController::class,'logout'])->name('logout');

/*******************PROFILE********************/
Route::get('/edit-profile',[ProfileController::class,'index'])->name('profile_index')->middleware('auth');
Route::post('/edit-profile',[ProfileController::class,'update'])->name('profile_update')->middleware('auth');

/*******************REGISTER********************/
Route::get('/register', [RegisterController::class,'index'])->name('register_index');
Route::post('/register', [RegisterController::class,'store'])->name('register_store');

/*******************HOME********************/
Route::get('/{user:username}',[PostController::class,'index'])->name('post_index');

/*******************POST********************/
Route::get('/posts/create',[PostController::class,'create'])->name('post_create')->middleware('auth');
Route::post('/posts',[PostController::class,'store'])->name('post_store')->middleware('auth');
Route::get('/{user:username}/posts/{post}',[PostController::class,'show'])->name('post_show');
Route::delete('/posts/{post}',[PostController::class,'destroy'])->name('post_destroy')->middleware('auth');

/*******************IMAGE********************/
Route::post('/images',[ImageController::class,'store'])->name('image_store')->middleware('auth');

/*******************COMMENTS********************/
Route::post('/{user:username}/posts/{post}',[CommentController::class,'store'])->name('comment_store')->middleware('auth');

/*******************LIKES********************/
Route::post('/posts/{post}/likes',[LikeController::class,'upsert'])->name('post_like_upsert')->middleware('auth');

/*******************FOLLOW********************/
Route::post('/{user:username}/follow',[FollowerController::class,'updateStatusFollow'])->name('upsert_follow');
