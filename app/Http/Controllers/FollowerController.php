<?php

namespace App\Http\Controllers;

use App\Models\Follower;
use App\Models\User;

class FollowerController extends Controller
{
    public function updateStatusFollow(User $user)
    {
        $followExists = Follower::where('user_id',$user->id)
            ->where('follower_id',auth()->user()->id)
            ->first();

        if($followExists){
            $new = $followExists;
            $new->active = !$new->active;
            $new->save();
        }else{
            $user->followers()->attach(auth()->user()->id);
        }

        return back();
    }
}
