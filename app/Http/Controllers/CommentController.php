<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request,User $user,Post $post)
    {
        $this->validate($request,[
            'comment'=>'required|max:255'
        ],[
            'comment.required'=>'El comentario es requerido.',
            'comment.max'=>'El comentario no puede tener más de 255 caracteres',
        ]);

        $comment = new Comment();
        $comment->comment = trim($request->input('comment'));
        $comment->user_id = auth()->user()->id;
        $comment->post_id = $post->id;

        if($comment->save()){
            return back()->with('message','Comentario realizado correctamente.');
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error, intente mas tarde '
            ],422);
        }

    }
}
