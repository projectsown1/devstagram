<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('profile.index');
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'username'=>'unique:users|min:3|max:30|regex:/^\S*$/',
            'image'=>'max:2048'
        ], [
            'username.unique' => 'El nombre de usuario ya está en uso.',
            'username.min' => 'El nombre de usuario debe tener al menos 3 caracteres.',
            'username.max' => 'El nombre de usuario no puede tener más de 30 caracteres.',
            'username.regex' => 'El nombre de usuario no debe contener espacios.',
            'image.max' => 'La imagen no debe superar los 2 MB',
        ]);

        $user = User::find(auth()->user()->id);
        $user->username = $request->username;

        $existingUser = Image::where('imageable_id', $user->id)->first();
        $imageContents = file_get_contents($request->image);
        $imageName = Str::uuid() . '.' . $request->file('image')->getClientOriginalExtension();
        $publicPath = public_path('profiles/' . $imageName);
        file_put_contents($publicPath, $imageContents);

        if($existingUser){
            $oldImagePath = $existingUser->url;
            if (file_exists($oldImagePath)) {
                unlink($oldImagePath);
            }
            $image = Image::find($existingUser->id);
        }else{
            $image = new Image();
        }

        $image->url = 'profiles/'.$imageName;
        $image->imageable_type = 'App\Models\User';
        $image->imageable_id = $user->id;

        $image->save();
        $user->save();



        return redirect()->route('post_index',$user->username);

    }
}
