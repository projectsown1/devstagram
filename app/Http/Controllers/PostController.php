<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['show','index']);
    }

    public function index(User $user)
    {
        $posts = Post::where('user_id',$user->id)
            ->with('image')
            ->latest()
            ->paginate(8);

        $totalPosts = Post::where('user_id',$user->id)
            ->count();

        return view('home',[
            'user' => $user,
            'posts' => $posts,
            'totalPosts' => $totalPosts,
        ]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|max:255',
            'description'=>'required',
            'image'=>'required|max:2048'
        ],[
            'title.required'=>'El título es requerido',
            'title.max'=>'El máximo de carácteres permitidos es de 255',
            'description.required'=>'La descripción es requerida',
            'image.required'=>'La imagen es requerida',
            'image.max' => 'La imagen no debe superar los 2 MB',
        ]);

        $post = new Post();
        $post->title = trim($request->input('title'));
        $post->description = trim($request->input('description'));
        $post->user_id = auth()->user()->id;

        $post->save();

        $imageUrl = 'uploads/' . $request->input('image');

        $image = new Image();
        $image->url = $imageUrl;
        $post->image()->save($image);

        return redirect()->route('post_index', ['user' => auth()->user()->username])
            ->with('success', 'Post creado exitosamente');
    }

    public function show(User $user,Post $post)
    {
        return view('posts.show',[
            'user'=>$user,
            'post'=>$post
        ]);
    }

    public function authorize($ability, $arguments=[])
    {
        return true;
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        $image = $post->image;

        $post->delete();

        if ($image && File::exists(public_path($image->url))) {
            unlink(public_path($image->url));
        }

        return redirect()->route('post_index',auth()->user()->username);
    }
}
