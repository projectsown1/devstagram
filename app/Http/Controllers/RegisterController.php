<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index () {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:30',
            'username'=>'required|unique:users|min:3|max:30|regex:/^\S*$/',
            'email'=>'required|unique:users|email|max:60',
            'password'=>'required|confirmed|min:8|regex:/[^\w]/',
        ], [
            'name.required' => 'El nombre es obligatorio.',
            'name.max' => 'El nombre no puede tener más de 30 caracteres.',
            'username.required' => 'El nombre de usuario es obligatorio.',
            'username.unique' => 'El nombre de usuario ya está en uso.',
            'username.min' => 'El nombre de usuario debe tener al menos 3 caracteres.',
            'username.max' => 'El nombre de usuario no puede tener más de 30 caracteres.',
            'username.regex' => 'El nombre de usuario no debe contener espacios.',
            'email.required' => 'El correo electrónico es obligatorio.',
            'email.unique' => 'El correo electrónico ya está registrado.',
            'email.email' => 'El correo electrónico debe ser una dirección válida.',
            'email.max' => 'El correo electrónico no puede tener más de 60 caracteres.',
            'password.required' => 'La contraseña es obligatoria.',
            'password.confirmed' => 'Las contraseñas no coinciden.',
            'password.min' => 'La contraseña debe tener al menos 8 caracteres.',
            'password.regex' => 'La contraseña debe tener al menos 1 carácter especial.',
        ]);

        $user = new User();
        $user->name = trim($request->input('name'));
        $user->username = trim($request->input('username'));
        $user->email = trim($request->input('email'));
        $user->password = bcrypt(trim($request->input('password')));
        $user->active = 1;

        if($user->save()){
            auth()->attempt([
                'email' => $request->email,
                'password' => $request->password
            ]);
            return redirect()->route('post_index', ['user' => $user->username]);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error, intente mas tarde '
            ],422);
        }
    }
}
