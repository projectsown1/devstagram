<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ],[
            'email.required' => 'El correo electrónico es obligatorio.',
            'email.email' => 'El correo electrónico debe ser una dirección válida.',
            'password.required' => 'La contraseña es obligatoria.'
        ]);

        if(!auth()->attempt($request->only('email','password'),$request->remember)){
            return back()->with('message','Credenciales incorrectas');
        }

        $user = auth()->user();

        return redirect()->route('post_index', ['user' => $user->username]);
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('login_index');
    }
}
