<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'username' => 'debugger',
                'email' => 'debugger@gmail.com',
                'password' => env('APP_DEBUG',true) ? Hash::make('prueba123') : Hash::make('@35T4GR4M'),
                'active' => true,
            ],
            [
                'name' => 'Prueba',
                'username' => 'prueba',
                'email' => 'prueba@gmail.com',
                'password' => env('APP_DEBUG',true) ? Hash::make('prueba123') : Hash::make('@35T4GR4M'),
                'active' => true,
            ]
        ]);
    }
}
